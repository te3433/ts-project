import { GameUnitType } from "../common/game-unit-type";
import { GameUnit } from "../models/game-unit.model";
import { Player } from "../models/player.model";

export class GameUnitFactory {
  public static createGameUnit(
    player: Player,
    health: number,
    armor: number,
    damage: number,
    unitType: GameUnitType
  ): GameUnit {
    if (player === null || player === undefined) {
      throw new Error("Invalid player");
    }
    const unit = new GameUnit(player, health, armor, damage, unitType);
    player.addUnit(unit);
    return unit;
  }
}

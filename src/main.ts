// // ForEach
// const forEach: (arr: number[], fn:(value: number) => void)=> void = (arr: number[], fn: (a: number) => void): void => {
//   for (const item of arr) {
//     // Call the function
//     fn(item);
//   }
// };

import { GameUnitType } from "./common/game-unit-type";
import { GameUnit } from "./models/game-unit.model";
import { Player } from "./models/player.model";
import { GameUnitFactory } from "./providers/game-unit-factory";

// // Filter
// const filter:  (arr: number[], checkFn:(a:number)=>boolean)=> number[] = (arr: number[], checkFn:(a:number)=>boolean): number[] =>{
//   const tempArr: number[] = [];
//   for (const item of arr) {
//     if (checkFn(item)) {
//       tempArr.push(item);
//     }
//   }

//   return tempArr;
// };

// // Map
// const map: (arr: number[], modifyFn:(a:number)=> number)=> number[] = (arr: number[], modifyFn:(a:number)=> number): number[] =>{
//   const tempArr: number[]= [];
//   for (const item of arr) {
//     const modified: number = modifyFn(item);
//     tempArr.push(modified);
//   }

//   return tempArr;
// };

// forEach([1,2,3], console.log)

// forEach(filter(map(
//   [1, 2, 3],
//   (item) => item * 5), // Function for map
//   (item) => item > 10), // Function for filter
//   console.log); // Function for forEach

const unitType: GameUnitType = GameUnitType.Zerg;

//console.log(unitType);

//console.log(new GameUnit(-1, 10, 10, GameUnitType.Protoss));

const player1: Player = new Player("Pencho");
const unit1 = GameUnitFactory.createGameUnit(
  player1,
  10,
  10,
  10,
  GameUnitType.Protoss
);
player1.addUnit(unit1);
unit1.health -= 11;

console.log(player1.armySize);

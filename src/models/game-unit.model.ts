import { GameUnitType } from "../common/game-unit-type";
import { Player } from "./player.model";

export class GameUnit {
  //health, armor, damage
  private _health: number;
  private _armor: number;
  private _damage: number;
  public readonly unitType: GameUnitType;

  constructor(
    private readonly player: Player,
    health: number,
    armor: number,
    damage: number,
    unitType: GameUnitType
  ) {
    this.health = health;
    this.armor = armor;
    this.damage = damage;
    this.unitType = unitType;
  }

  set health(value: number) {
    if (value <= 0) {
      value = 0;
      this.player.removeUnit(this)
      console.log("dead");
    }
    this._health = value;
  }

  get health(): number {
    console.log("getting health");
    return this._health;
  }

  set armor(value: number) {
    if (value <= 0) {
      throw new Error("Health can't be 0 or below!");
    }
    this._armor = value;
  }

  get armor(): number {
    return this._health;
  }

  set damage(value: number) {
    if (value <= 0) {
      throw new Error("Health can't be 0 or below!");
    }
    this._damage = value;
  }

  get damage(): number {
    return this._damage;
  }
}

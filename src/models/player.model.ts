import { GameUnit } from "./game-unit.model";

export class Player {
  private static count: number = 0;
  public readonly id: number;
  private army: GameUnit[] = [];

  constructor(private readonly _name: string) {
    if (_name.length === 0) {
      throw new Error("Please, provide a valid name");
    }
    this.id = Player.count++;
  }

  public get name(): string {
    return this._name;
  }

  public get armySize(): number {
    return this.army.length;
  }

  public addUnit(unit: GameUnit): void {
    if (unit === undefined || unit === null) {
      throw new Error("Not a valid unit!");
    }
    if (this.army.find((u) => u === unit)) {
      throw new Error("This unit is already in the army");
    }
    this.army.push(unit);
  }

  public removeUnit(unit: GameUnit): void {
    if (unit === undefined || unit === null) {
      throw new Error("Not a valid unit!");
    }
    this.army = this.army.filter((u) => u !== unit);
  }
}

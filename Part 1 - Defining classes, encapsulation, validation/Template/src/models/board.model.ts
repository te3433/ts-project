import { Task } from "./task.model";

export class Board {
  public static INSTANCE(): Board {
    if (Board._instance === undefined) {
      Board._instance = new Board();
    }
    Board._taskCount++;
    return Board._instance;
  }
  private _tasks: Task[] = [];
  private static _taskCount: number = 0;
  private static _instance: any;


  private constructor() {
    
  }

  get tasks() {
    return this._tasks;
  }

 
  public get taskCount(): number {
    return Board._taskCount;
  }


  public add(task: Task): void {
    if (task === undefined || task === null) {
      throw new Error("Not a valid task!");
    }
    if (this._tasks.find((t) => t === task)) {
      throw new Error("This task is already in the Board");
    }
    this._tasks.push(task);
  }
  public remove(task: Task): void {
    if (task === undefined || task === null) {
      throw new Error("Task does not exists!");
    }
    this._tasks = this._tasks.filter((t) => t !== task);
  }


  public toString() {
    if (Board._taskCount < 1) {
      return `---Task Board---

    Tasks:
    

    No tasks at the moment.
    
    `;
    } else {
      return `---Task Board---

    Tasks:
    

    ${this._tasks}
    
    `;
    }
  }
}

import { TaskStatus } from "../common/task-status";
import moment from 'moment';

export class Task {
  private _name: string;
  private _due: Date;

  private static _status: TaskStatus = TaskStatus.Todo;

  constructor(value: string, date: Date) {
    this.name = value;
    this.due = date;
    
  }

  public get name(): string {
    return this._name;
  }
  public set name(value: string) {
    if (!value) {
      throw new Error("Name should be valid");
    }
    if (value.length < 6 || value.length > 20) {
      throw new Error("Name length should be between 6 and 20 characters");
    }
    this._name = value;
  }

  public get due(): Date {
    return this._due;
  }
  public set due(value: Date) {
    this._due = value;
  }

  public reset(): void {
    Task._status = TaskStatus.Todo;
  }

  public advance(): void {
    Task._status = TaskStatus.InProgress;
  }

  public complete(): void {
    Task._status = TaskStatus.Done;
  }

  public toString() {
    return `\n Name: ${this.name}\n
    Status: ${Task._status}\n
    Due: ${moment(this.due).subtract(10, 'days').calendar()}\n`;
  }
}

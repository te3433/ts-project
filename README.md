<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

## TypeScript Starter

1. Run ```npm install```
1. There are a few scripts you could run

    - ```npm run start``` - run the project
    - ```npm run test``` - run the tests
    - ```npm run majestic``` - run the tests with [Majestic](https://github.com/Raathigesh/majestic)
    - ```npm run lint -s``` - lint the project
    - ```npm run lint:fix -s``` - fix the problems with lint

    - The ```-s``` flag in lint means to run it silent and not report back exceptions


